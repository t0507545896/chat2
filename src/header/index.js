import React from 'react';
import styles from './styles.module.css';
import classnames from 'classnames';

const Header = (props) => {
    const { className, chatName, participants, messages, messageTime } =  props;
    console.log(`clName => `, className);
    const combinedHeaderCss = classnames([className, styles.header_bar]);
    return (
        <div className={combinedHeaderCss}>
            <div className={styles.chatName}>{chatName}</div>
            <div className={styles.participants}>{participants} participants</div>
            <div className={styles.messages}>{messages} messages</div>
            <div className={styles.time}>last messages at {messageTime}</div>

        </div>
    );
}

export default Header;