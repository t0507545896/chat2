import React from 'react';
import Header from '../header';
import styles from './styles.module.css';
import Logo from '../logo';
import MessageList from '../message_list';
import MessageInput from '../message_input';
import { v4 as uuid } from 'uuid';
import moment from 'moment';

const myUserId = 'the_best_code_ever';
const myUserName = 'SuperStar';
const myAvatar = '';

function createMyNewMessage(messageText) {
  const nowTime = moment.utc().format();
  return {
    id: uuid(),
    text: messageText,
    user: myUserName,
    avatar: myAvatar,
    userId: myUserId,
    editedAt: nowTime,
    createdAt: nowTime,
    isMyMessage: true
  }
}


class Chat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      ongoingInputText: ''
    };
  }

  componentDidMount() {
    this.getAvailableMessages()
      .then(messagesData => {
        const changedMessages = (messagesData || []).map(msg => {
          const isMyMessage = msg.userId === myUserId;
          return {
            ...msg,
            isMyMessage: isMyMessage
          };
        });

        console.log(`messages received size=${messagesData}`);
        this.setState({
          messages: changedMessages
        });
      })
  }

  componentWillUnmount() {
  }

  // returns Promise of [Message]
  getAvailableMessages() {
    const request = new Request('https://api.npoint.io/b919cb46edac4c74d0a8', {
      method: 'GET'
    });
    return fetch(request)
      .then(response => response.json())
  }

  render() {
    const { messages } = this.state;

    const onInputTextChange = (currentText) => {
      this.setState({
        ongoingInputText: currentText
      });
    }

    const onSendButtonClicked = () => {
      const messageText = this.state.ongoingInputText;
      const newMessage = createMyNewMessage(messageText);
      console.log(`New msg => `, newMessage);
      const oldMessagesList = this.state.messages;
      const newMessagesList = [...oldMessagesList, newMessage];
      this.setState({
        messages: newMessagesList,
        ongoingInputText: ''
      });
    }

    return (
      <>
        <Logo />
        <div className={styles.content_container}>
          <Header className={styles.header_content} chatName="My Chat" participants={23} messages={53} messageTime={23.15} />

          <MessageList messages={messages} />

          <div className={styles.message_bar}>
            <MessageInput text={this.state.ongoingInputText} onTextChanged={onInputTextChange} />
            <button className={styles.send_btn} onClick={() => {
              onSendButtonClicked()
            }}>Send</button>
          </div>
        </div>

      </>
    );
  }

}

export default Chat;

