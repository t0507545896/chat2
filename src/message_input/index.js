import React from 'react';
import style from './style.module.css';

const MessageInput = (props) => {

    const { text, onTextChanged }  = props;

    return (
        <>
            <input type="text" value={text} placeholder="Message"  className={style.message_input} onChange={(event) => {
                const text = event.target.value;
                onTextChanged(text);
                }}></input>            
        </>
    )
}
export default MessageInput;
