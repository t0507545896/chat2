import React from 'react';
import styles from './styles.module.css';
import MessageItem from '../message_item';

const MessageList = (props) => {
    const { messages } = props;
    
    const messageItems = [];
    for (let msg of messages) {
        const isMyMessage = msg.isMyMessage;
        const messageItem = isMyMessage ? (null) : (<MessageItem key={msg.id} className={styles.item_spacing} message={msg}/>);
        messageItems.push(messageItem);
    }

    return (
        <div className={styles.messages_container}>
            {messageItems}
        </div>
    )
}

export default MessageList;
